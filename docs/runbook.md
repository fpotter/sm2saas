# Runbook

EEverything you need to perform a self-managed to SaaS migration with direct transfer and Congregate
e

_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

---

Use this as a starting point for a migration runbook (copy/paste to an issue)

- [ ] Check [status](https://status.gitlab.com/)
- [ ] Release pre-migration communication
- [ ] Add a banner to source instance - NOT MAINTENANCE MODE!
- [ ] Set user email addresses on the source instance to "public"
- [ ] If neceessary, adjust rate limits
    * [Project/group import/export rate limits](https://docs.gitlab.com/ee/user/admin_area/settings/import_export_rate_limits.html)
    * [GitLab self-managed User and IP rate limits](https://docs.gitlab.com/ee/user/admin_area/settings/user_and_ip_rate_limits.html)
    * [GitLab self-managed application limits](https://docs.gitlab.com/ee/administration/instance_limits.html)
    * General [Rate limits](https://docs.gitlab.com/ee/security/rate_limits.html)
- [ ] Complete all provisioning, setup, and pilot migrations all complete
- [ ] Start the containers - `do/up`
- [ ] List and stage the content
    - `do/dt-off`
    - `do/dkr-congregate`
    - `congregate list`
    - `congregate stage-users . --commit`
    - `congregate set-staged-users-public-email`
    - `congregate stage-groups . -–commit`
    - `exit`
- [ ] Perform the migration
    - `do/dt-on`
    - `do/dkr-congregate`
    - `supervisorctl start all`
    - https://localhost:8000
    - Skip users > Migrate
    - (confirm dry run listing)
    - Skip users > Commit > Migrate
    - (OK to exit and return later)
    - (wait)
- [ ] Perform post-migration steps
    - (return to the container if necessary)
    - `supervisorctl stop all`
    - `exit`
    - `do/dt-off`
    - `do/dkr-congregate`
    - `nohup congregate migrate --only-post-migration-info --skip-users --commit > data/logs/post-migration.log 2>&1 &`
    - (OK to exit and return later)
- [ ] Run a diff report
    - `congregate generate-diff --staged --skip-groups --skip-users`
- [ ] Proceed with post-migration validation steps
- [ ] Release post-migration communication
- [ ] Turn on maintenance mode in source instance

<br/><br/>

See [overview](overview.md) to get started.

[Contribute](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/edit/main/-/) or submit [issues](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/boards).