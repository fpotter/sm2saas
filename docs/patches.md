# Patches

Everything you need to perform a self-managed to SaaS migration with direct transfer and Congregate

_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

---

Consider some of the patches below before learning the commands. These changes MIGHT not be required, and if they are, then calibration of actual values requires trial-and-error.

## Sidekiq memory

We had problems with Sidekiq running out of memory while exporting batches on a single-node Omnibus instance. Some of the following changes might help.

### Increase overall memory

Increase memory on the source instance. Here's where we got with one client. Differs in multi-node instances.

```
root@gitlab-vm-01:~# free -h
              total        used        free      shared  buff/cache   available
Mem:          117Gi        29Gi       9.4Gi       246Mi        79Gi        87Gi
Swap:            0B          0B          0B
```

### Increase memory for Sidekiq processes

In `/etc/gitlab/gitlab.rb`:

```ruby
sidekiq['env'] = {
    "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => "8000000"
}
gitlab_rails['env'] = {
    "SIDEKIQ_MEMORY_KILLER_MAX_RSS" => "8000000"
}
```

(only one of those is necessary but which one? probably the first)

Then `sudo gitlab-ctl reconfigure`

### Decrease Sidekiq concurrency

In `/etc/gitlab/gitlab.rb`:

```ruby
sidekiq['concurrency'] = 10
```

Then `sudo gitlab-ctl reconfigure`

(was commented out but defaulted to 20)


### Decrease relations export batch size

In `/opt/gitlab/embedded/service/gitlab-rails/app/services/bulk_imports/batched_relation_export_service.rb`:

```ruby
BATCH_SIZE = 500
```

(was 1000)

Then `sudo gitlab-ctl restart sidekiq`

### Increase Sidekiq retries

A user on another project found a way to [increase Sidekiq retries](https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/issues/171#note_2034708227) for relation export batches.

Doing so requires a manual patch to `/app/workers/bulk_imports/relation_batch_export_worker.rb`

by inserting this line in the correct place:

```ruby
sidekiq_options max_retries_after_interruption: 20
```

Then `sudo gitlab-ctl restart sidekiq`

Notes for the curious:

- The [default is 3](https://gitlab.com/gitlab-org/gitlab/-/blob/master/vendor/gems/sidekiq-reliable-fetch/lib/sidekiq/base_reliable_fetch.rb#L22)
- This all happens inside the "Reliable Fetcher" gem, previously a separate project but now "vendored" inside of the GitLab code base.

_We didn't actually try this but noted it_

## Congregate patches

If the MRs have landed, then not necessary.

- [NPM packages](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/merge_requests/1025)
- [Protected environment settings](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/merge_requests/1029)

<br/>
<br/>

**Next: Learn the [commands](commands.md)**

[Contribute](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/edit/main/-/) or submit [issues](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/boards).