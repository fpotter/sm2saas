# Overview

Everything you need to perform a self-managed to SaaS migration with direct transfer and Congregate

_(Might help with other DT/Congregate scenarios too)_

_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

---

## Assumptions

- GitLab self-managed to multitenant SaaS migration
- Planning to use direct transfer and Congregate
- Professional services engineer
- Appropriate access to both systems*
- Ability to provision a migration VM with network connectivity
- Familiarity with Congregate and direct transfer principles

## Can we do this in the GitLab UI?

Sort of. The direct-transfer-over-Congregate approach (documented here) uses the same underlying mechanism as [Direct transfer in the GitLab UI](https://docs.gitlab.com/ee/user/group/import/direct_transfer_migrations.html) except that it allows tighter control over the steps and includes the "post-migration" activities to copy other types of data beyond those covered in the UI. So in general, we recommend direct-transfer-over-Congregate for professional services engagements.

## Can we do this with export/import?

Yes! Doing so might offer some advantages, such as the ability to inspect and modify the data before importing it, or to copy it out of an airgapped environment. But it also might take longer. This toolkit covers the direct-transfer-over-Congregate approach.

## Does it require an "admin token"?

On the source (self-managed) instance, a user with admin privileges is best, in addition to shell access to the GitLab instance for troubleshooting.

On `GitLab.com`, an admin token is only required for two reasons:

1. If you need to create new user records, instead of relying on SCIM auto-provisioning
1. If you ever need to cancel the bulk import operation midway because you know it's failing

In either case (admin or non-admin), the user in question should be especially provisioned for the migration operation and must have Owner privileges on the top-level group in question.


## Tools

The delivery kit contains shortcuts and templates to make work easier, documented herein.

1. Provision migration VM
1. Install Docker
1. `curl https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/raw/main/install | bash`
1. `do/clean`
1. Edit `settings.sh` with your settings
1. Use the `do/` scripts to drive action

Hot tip: Add a little script to connect to the source GitLab instance over SSH from the miration VM. Here was ours:

```
#!/bin/bash
gcloud beta compute --project "myproject" ssh --zone "us-west2-a" "gitlab-vm-01" --tunnel-through-iap
```

<br/>
<br/>

**Next: consider [patches](patches.md)**

[Contribute](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/edit/main/-/) or submit [issues](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/boards).