# Troubleshooting

Everything you need to perform a self-managed to SaaS migration with direct transfer and Congregate


_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

---

Things will go wrong. Here's what to do. Note that the Congregate UI and it's Celery sidekiq offer little information about what's happening under the hood, because the real action is in the two GitLab instances.

## Watch Sidekiq on the source instance

Sign into the source GitLab instance and go to Admin Area > Monitoring > Background Jobs. Click "Busy" on the Sidekiq page. Places to keep your eyes:

- Processes/RSS - That's the memory consumption. If it increases over time, you might have a problem. When it hits the [limit](patches.html#increase-memory-for-sidekiq-processes) the processes will restart.
- Processes/Busy - Tells you how many threads are being used. Unfortutely it seems to get a little out of balance sometimes, with all the long-running jobs ending up on a small number of threads. The limit is the [concurrency](patches.html#decrease-sidekiq-concurrency) I believe.
- Jobs - Look for jobs called `RelationExportWorker` and `RelationBatchExportWorker` that do the heavy lifting of generating the exports used by direct transfer. They should come and go as the batches run. See commands below to get details on each worker.
- Jobs - Also look for jobs called `FinishBatchedRelationExportWorker` (or other titles containing the word "finish") - these seems to watch for the export worker jobs and then run. But if the export workers time out (as in with memory overruns), they might get left hanging and appear, then disappear, as you watch. Also note that the job IDs are the same on each successive run of the same type of object, so you'll see them reused.

To inspect the code that's running there, look under [bulk_imports](https://gitlab.com/gitlab-org/gitlab/-/tree/master/app/workers/bulk_imports) for the applicable GitLab version.

Note: "Background jobs" in the context of Sidekiq is different from "job" in GitLab CI/CD.

---

## View details of batches on the source instance

Some of the relation types (such as pipelines) might run for much longer than others and come in batches. Use the [do/ scripts](commands.html#scripts) in this toolkit to get more detail.

### Get the status (use the project id)
```bash
do/api-source /projects/${PROJECT_ID}/export_relations/status > temp/direct-transfer-source-relations-export-status.json
```

### Big magic query to see which relations are still running and how many batches of each
```bash
cat temp/direct-transfer-source-relations-export-status.json | jq '[.[] | select(.status == 0) | {relation, batches: (if has("batches") then (.batches | map(select(.status == 0)) | length) else null end)}]'
```

### ... which is so useful we put it into one script (provide a project ID)

```bash
do/query-source-batches 8
```

---

## View details on the target instance (i.e. GitLab.com)

If you need to get more details on the target instance, follow the commands below. Note that they require that the user making the query be the user that started the export (i.e. the same token used by Congregate).

### See the running import

Hopefully the user is only running one import at a time.

```bash
do/api-target bulk_imports | jq -r '.[] | select(.status=="started")'
```

### Capture the ID

Just a handy way to hold the bulk import ID locally for the other queries.

```bash
do/api-target bulk_imports | jq -r '.[] | select(.status=="started") | .id' > temp/.bulk-import-id
```

### See the overall status
```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id) | jq
```

### All the entities, counted by status, only works if <100

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?per_page=100 | jq -r '.[] | .status' | sort | uniq -c
```

### Entities started

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?status=started | jq -r '.[] | .source_full_path'
```

### Entities timed out

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?status=timeout | jq -r '.[] | .source_full_path'
```

### Entities failed

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?status=failed | jq -r '.[] | .source_full_path'
```

## Build a file of all the activity data from the target (i.e. GitLab.com)

Run this query to get all the details from the SaaS perspective. Assumes a `.bulk-import-id` (above). (Note this data will change so re-run as needed.)


```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?per_page=100\&page=1 > temp/entities-latest
```

If the result was < 100 entries, get more. Keep incrementing `page` until you see <100 entries.

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities\?per_page=100\&page=2 >> temp/entities-latest
```

Clean up the final list since we just performed a crude join of JSON output.

```bash
sed -i 's/\]\[/,/g' temp/entities-latest
```

## Dig into the SaaS status information

Assumes `entities-latest` (above)


### Count the entities

```bash
cat temp/entities-latest | jq -r '.[] | .source_full_path' | wc -l
```

### Count entities by status

```bash
cat temp/entities-latest | jq -r '.[] | .status' | sort | uniq -c
```

### See the time of the latest entity update

```bash
cat temp/entities-latest | jq -r '.[] | .updated_at' | sort | tail -n 1
```

### See who has failures

```bash
cat temp/entities-latest | jq '[.[] | select(.has_failures == true)] | .[] | .source_full_path'
```

### See what kinds of failures

```bash
cat temp/entities-latest | jq '[.[] | select(.has_failures == true)] | .[] | {source_full_path,failures:.failures[] | {relation}}'
```

### A different filter

```bash
cat temp/entities-latest | jq '[.[] | select(.status=="finished")] | .[] | {source_full_path,failures:.failures[] | {relation}}'
```

### Some other failure-related queries

```bash
cat temp/entities-latest | jq -r '[.[] | select(.status=="finished")] | .[] | select(all(.failures[];.relation!="members")) | .source_full_path'
```

```bash
cat temp/entities-latest | jq '[.[] | select(.status=="finished")] | .[] | {source_full_path,failures:.failures[] | {relation}}'
```

```bash
cat temp/entities-latest | jq -r '[.[] | select(.status=="finished" and (.failures | length > 0))] | .[] | select(all(.failures[];.relation!="members")) | {source_full_path,failures:.failures[] | {relation}}'  # not working?
```

```bash
cat temp/entities-latest | jq -r '[.[] | select(.status=="finished")] | .[] | select(any(.failures[];.relation=="merge_requests")) | .source_full_path'
```


### Failure details
```bash
cat temp/entities-latest | jq '[.[] | select(.has_failures == true)] | .[] | select(all(.failures[];.relation!="snippets_repository")) | {source_full_path,failures}'
```

### Projects with snippets_repository failures
```bash
cat temp/entities-latest | jq -r '[.[] | select(.has_failures == true)] | .[] | select(all(.failures[];.relation=="snippets_repository")) | .source_full_path'
```

### List ones still running
```bash
cat temp/entities-latest | jq -r '[.[] | select(.status != "finished")] | .[] | .source_full_path'
```

## A few more SaaS tricks

### Live update on a specific entity

Give this a specific entity ID

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/entities/${ENTITY_ID} | jq
```

### List STAGED group projects

This is looking into what you did with Congregate

```bash
cat data/staged_projects.json| jq -r '[.[] | select(.project_type == "group")] | .[] | .path_with_namespace'
```

---

## Canceling


### Cancel in SaaS

Requires admin privileges - ALSO must be the same user who started the import.

```bash
do/api-target bulk_imports/$(cat temp/.bulk-import-id)/cancel --request POST
```


### Cancel on source

Requires Rails console access.

Check the ID

```ruby
BulkImports::Export.where('status=0')
```

Then cancel

```ruby
e=BulkImports::Export.find_by_id(2002)
e.update(status: -1)
e.save!
```

(bonus points for doing so in one line using a Ruby block)

---

## Post-migration results data

From Congregate, after running "post-migration" step.

List of projects (?)
```bash
cat data/results/project_migration_results.json | jq -r '.[] | keys[0]'
```

A specific project
```bash
more data/results/project_migration_results.json | jq '.[] | select(has("__target_path__"))'
```

Errors in logs
```bash
cat data/logs/post-migration.log| grep '\[ERROR\]'
```

How many of each type of error
```bash
cat data/logs/post-migration.log | egrep '\[ERROR\]|.*|' | sed -n 's/.*\[ERROR\]|\(.*\)|.*/\1/p' | sort | uniq -c | sort -r
```

---

## Evaluate

`do/dkr-evaluate` then:

```bash
evaluate-gitlab -t $SOURCE_TOKEN -s https://$SOURCE_HOSTNAME
```

---

## Troubleshooting MongoDB

```bash
# in terminal
docker exec -it congregate_mongo mongo

# you should now be in the mongo shell
use congregate
show dbs
db['projects-<customer-src-url-without-https>'].count()
db['projects-<customer-src-url-without-https>'].find()
```

---

## Kibana

[https://log.gprd.gitlab.net/](https://log.gprd.gitlab.net/)

### Methods for Finding Import Logs

#### Failed Relations

- Make sure you are looking at the production rails metrics
- Search on `json.meta.caller_id: POST /api/:version/projects/import-relation AND json.meta.user_id: <user ID for the token that started the import goes here>`
  - That will give you a json.correlation_id
- Take that to the production sidekiq metrics
  - The earliest item in that search should have a json.jid
- Search on that `json.jid: <your json.jid>`
- You can focus on the various json.exception.* fields to look for errors

---

## GitLab logs

Also useful to look into the sidekiq logs on the source instance. Shell in then start here:

```bash
sudo tail /var/log/gitlab/sidekiq/current
```

Note that most log lines are JSON (have fun with JQ) but some are not -- specifically, the log lines about low memory and restarts. If you happen to find them and can paste the exact text here, that might help future engineers.

<br/><br/>

**Next: Follow the [runbook](runbook.md)**

[Contribute](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/edit/main/-/) or submit [issues](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/boards).