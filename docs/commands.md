# Commands

Everything you need to perform a self-managed to SaaS migration with direct transfer and Congregate


_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

---

Understand the commands below before following the runbook. This is just a list. Assumes [installation](overview.html#tools)

## Scripts

- `do/configure` - Copy settings into the conf template (including base64 encoding of tokens) and drop it in the `data` directory
- `do/up` - Start everything via Docker Compose
- `do/down` - Stop everything
- `do/dkr-congregate` - Shell into the congregate container (assumes you already ran `do/up`)
- `do/dkr-evaluate` - (Might need some work) Shell into the evaluate container
- `do/api` - Call REST API endpoint on the source instance (provide endpoint and query parameters for use in curl)
- `do/dt-get` - Check direct transfer setting from the conf
- `do/dt-on` - Turn on direct transfer
- `do/dt-off` - Turn off direct transfer
- `do/listed-groups` - Show the groups that are listed
- `do/listed-projects` - Show the projects that are listed
- `do/listed-project` - Give it a project to get details
- `do/pull` - Pull the latest version of this delivery kit, to get updates
- `do/staged-groups` - Show the groups that are staged
- `do/staged-projects` - Show the projects that are staged


## Container commands

_Note that direct transfer migrations must be triggered through the Congregate UI, through port forwarding. But every step before and after it can happen in the CLI on the VM. So we provide an easy way to turn on and off the DT setting in the Congregate config._

Examples of commands that run inside the Congregate container (i.e. after `do/dkr-congregate`):

With `do/dt-off`:

- `congregate init`
- `congregate list`
- `congregate stage-users . --commit`
- `congregate set-staged-users-public-email --commit`
- `congregate stage-groups . --commit > stage-groups.log 2>&1`

With `do/dt-on`:

- `congregate validate-config`
- `supervisorctl start all`
- `supervisorctl stop all`

Post-migration with `do/dt-off`:

```bash
nohup congregate migrate --only-post-migration-info --commit > log.log 2>&1 & 
```

<br/><br/>

**Next: Prepare for [troubleshooting](troubleshooting.md)**

[Contribute](https://gitlab.com/-/ide/project/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/edit/main/-/) or submit [issues](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas/-/boards).