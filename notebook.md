
# 20241017

Useful updates

- Migration VM - useful to have `sudo` access to install `apt` packages for convenient tools (in my case, zsh and emacs)
- `TARGET_GROUP_ID` and `TARGET_GROUP_PATH` in SM2SM case?
- Migration requires docker compose (actually a plugin to docker engine on linux)
