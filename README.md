# SM2SaaS delivery kit

:rotating_light: :rotating_light: :rotating_light:

**Please Note:** This project has been archived, and its content has been moved over to the [sm2saas folder within the Migration Delivery Kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-delivery-kits/migration-delivery-kit/-/tree/main/Congregate/sm2saas?ref_type=heads).


_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_

https://gitlab-org.gitlab.io/professional-services-automation/delivery-kits/migration-delivery-kits/sm2saas